<?php
include("app/login.php");
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Books Manager</title>
        <link rel="stylesheet" href="public/css/style.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="public/js/script.js"></script>
    <body>
        <div class="login">
        <h3 align="center">Welcome to Books Manager</h3>
        <h4>Login please</h4>
        <form action="" method="post" style="text-align:center;">
        <input type="text" placeholder="Username" id="user" name="user"><br/><br/>
        <input type="password" placeholder="Password" id="pass" name="pass"><br/><br/>
        <input type="submit" class="btn btn-info btn-lg" value="Login" name="submit">
        <!-- Error Message -->
        <span><?php echo $error; ?></span>
    </body>
</html>