var booksArray = [];
var selectedIndex = -1;
var reg_pusto = new RegExp('([^\\s*]+)','g');

function sort() {
  $("#tablerows").html("");
  if (localStorage.booksRecord) {
      booksArray = JSON.parse(localStorage.booksRecord);
      for (var i = 0; i < booksArray.length; i++) {
          prepareTableCell(i, booksArray[i].book, booksArray[i].author, booksArray[i].user, booksArray[i].date);
      }
  }
  hide();
}

function sortId() {
	$("#tablerows").html("");
  if (localStorage.booksRecord) {
      booksArray = JSON.parse(localStorage.booksRecord);
      var length = booksArray.length;
      for (var i = length-1; i >= 0; i--) {
          prepareTableCell(i, booksArray[i].book, booksArray[i].author, booksArray[i].user, booksArray[i].date);
      }
  }
  hide();

}

function hide(){
  $('#edit').addClass('hidden');
	$('#delete').addClass('hidden');
  $('#edit').attr('data-id',-1);
  $('#delete').attr('data-id',-1);
}

function prepareTableCell(index, book, author, user, date) {
  var row = '<tr id="'+index+'" >\n\
    <td class="Id">'+ index +'</td><td class="Book">'+ book +'</td><td class="Author">'+ author +'</td><td class="User">'+ user +'</td>\n\
    <td class="Date">'+ date +'<input class="hidden" type="radio" name="optradio" id="check_'+ index +'"></td>\n\
    </tr>';
  $('#tablerows').append(row);
}

$(document).ready(function(){

  $("#save").on('click', function(){
    var book = $("#book_name").val();
    var author = $("#author").val();
    var user = $("#user").val();
    var d = new Date();
    var date = d.getDate() + '.' + d.getMonth()+1 +'.'+ d.getFullYear()+
          ' '+ d.getHours() +':'+ d.getMinutes() +':'+ d.getSeconds();
    var stuObj = {book: book, author: author, user: user, date:date};
    if($("#book_name").val().replace(/\s+/g, '').length && $("#author").val().replace(/\s+/g, '').length && $("#user").val().replace(/\s+/g, '').length){
      if (selectedIndex === -1) {
          booksArray.push(stuObj);
      } else {
          booksArray.splice(selectedIndex, 1, stuObj);
      }
     	console.log(booksArray.book);
      localStorage.booksRecord = JSON.stringify(booksArray);
      sort();
      $('#adding').modal('toggle');
      $('#add_book').trigger('reset');
    } else {

    }

  });

  $('#adding').on('hidden.bs.modal', function () {
    $("#book_name").val('');
    $("#author").val('');
    $("#user").val('');
  });

  $('#editing').on('hidden.bs.modal', function () {
    $("#edit_book_name").val('');
    $("#edit_author").val('');
    $("#edit_user").val('');
  });

  $(document).on('click','tbody tr',function() {
  	$('tbody tr').css('background-color','white');
  	var value = $(this).attr("id");
    if ($( "#check_"+value ).is(':checked')) {
    	$(this).css('background-color','white');
    	$( "#check_" + value ).prop( "checked", false );
    	hide();
    }
    else {
    	$(this).css('background-color','rgba(255,255,111,0.4)');
    	$( "#check_"+value ).prop( "checked", true );
    	$('#edit').removeClass('hidden');
    	$('#delete').removeClass('hidden');
      $('#edit').attr('data-id',value);
      $('#delete').attr('data-id',value);
    }
  });

  $('#edit').on('click', function() {
      selectedIndex = $('#edit').attr('data-id');
      var stuObj = booksArray[selectedIndex];
      $("#edit_book_name").val(stuObj.book);
      $("#edit_author").val(stuObj.author);
      $("#edit_user").val(stuObj.user);
  });

  $("#do_edit").on('click', function(){
      var book = $("#edit_book_name").val();
      var author = $("#edit_author").val();
      var user = $("#edit_user").val();
      var d = new Date();
      var date = d.getDate() + '.' + d.getMonth()+1 +'.'+ d.getFullYear()+
            ' '+ d.getHours() +':'+ d.getMinutes() +':'+ d.getSeconds();
      selectedIndex = $('#edit').attr('data-id');
      if($("#edit_book_name").val().replace(/\s+/g, '').length && $("#edit_author").val().replace(/\s+/g, '').length && $("#edit_user").val().replace(/\s+/g, '').length){
	      var stuObj = {book: book, author: author, user: user, date: date};
	      if (selectedIndex === -1) {
	          booksArray.push(stuObj);
	      } else {
	          booksArray.splice(selectedIndex, 1, stuObj);
	      }
	      localStorage.booksRecord = JSON.stringify(booksArray);
	      sort();
	      $('#editing').modal('toggle');
    	} else {

    	}

  });

  $("#delete").on('click', function(){
		if(confirm("Are you sure?")===true){
      var id = $('#delete').attr('data-id');
      booksArray.splice(id, 1);
      localStorage.booksRecord = JSON.stringify(booksArray);
      sort();
    }
  });
});
